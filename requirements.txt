cassandra-driver >= 3.19.0, != 3.21.0
click
deprecated
flask
iso8601
mypy_extensions
psycopg2
redis
tenacity >= 6.2, != 8.2.0  # https://github.com/jd/tenacity/issues/389
typing-extensions
